package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void  testEcho(){
        App a = new App();
        int i = 5;
        assertEquals("testing echo", i, a.echo(i));
    }

    @Test
    public void  testOneMore(){
        App a = new App();
        int i = 5;
        assertEquals("testing echo", i + 1, a.oneMore(i));
    }
}
